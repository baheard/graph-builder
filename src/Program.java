
import graphBuilder.MainPainter;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

import algorithms.GrahamScanAlgorithm;
import algorithms.QuickGrahamScanAlgorithm;

public class Program {

	JFrame f;
	JPanel p;
	MainPainter graph;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPainter window = new MainPainter(700,700);
					window.setVisible(true);
					window.addAlgorithm(new GrahamScanAlgorithm());
					window.addAlgorithm(new QuickGrahamScanAlgorithm());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
