package graphBuilder;

//import main;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainPainter extends JFrame{
	
	int height;
	int width;
	JPanel mainPanel;
	GraphComponent graphPanel;
	ControlComponent controlPanel;
	
	public MainPainter(int width, int height){
		this.height = height;
		this.width = width;
		init();
	}

	public void init(){
		this.getContentPane().setBackground(Color.WHITE);
		mainPanel = new JPanel();
		graphPanel = new GraphComponent(width, height);
		controlPanel = new ControlComponent(graphPanel);
		this.getContentPane().add(graphPanel, BorderLayout.CENTER);
		this.getContentPane().add(controlPanel, BorderLayout.EAST);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
//		while(true);
	}
	
	public void addAlgorithm(GraphAlgorithm newAlgorithm){
		controlPanel.addAlgorithm(newAlgorithm);
	}
	public GraphComponent getGraph(){
		return graphPanel;
	}
}