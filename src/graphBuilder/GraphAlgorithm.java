package graphBuilder;

public abstract class GraphAlgorithm {
	protected String name;
	
	public GraphAlgorithm(String name){
		this.name = name;
	}
	
	public abstract void runAlgorithm(GraphComponent graph);
	
	@Override
	public String toString(){
		return this.name;
	}
}
