package graphBuilder;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

import algorithms.SimplePoint;

public class ControlComponent extends JPanel {

	final static Color SILVER = new Color(220,220,220);
	final static Color LIGHT_BLUE = new Color(245,245,255);
	
	protected GraphComponent graphPanel;
	
	protected JPanel clearPanel;
	protected JPanel pointPanel;
	protected JPanel linePanel;
	private int historyCounter;
	
	protected JTextField xPointText;
	protected JTextField yPointText;
	protected JTextField x1LineText;
	protected JTextField y1LineText;
	protected JTextField x2LineText;
	protected JTextField y2LineText;
	protected JTextArea statusHistory;
	protected JComboBox algorithmsCombo; 
	protected List<GraphAlgorithm> graAlgs;
	
	public ControlComponent(GraphComponent graphPanel){
		this.graphPanel = graphPanel;
		this.setLayout(new GridLayout(5, 1));
		historyCounter = 1;
		init();
		graAlgs = new ArrayList<GraphAlgorithm>();
	}
	public void addAlgorithm(GraphAlgorithm newAlgorithm){
		algorithmsCombo.addItem(newAlgorithm);
		
	}
	
	private void init(){
		this.setBorder(BorderFactory.createBevelBorder(0, Color.LIGHT_GRAY, Color.DARK_GRAY));
		this.setPreferredSize(new Dimension(new Dimension(150, 200)));
		
		this.add(createPointPanel());
		this.add(createLinePanel());
		this.add(createRandPanel());
		this.add(createAlgPanel());
		this.add(createStatusHistoryPanel());
		
		graphPanel.setTextFields(xPointText, yPointText, x1LineText, y1LineText, x2LineText, y2LineText);
	}

	private String getHistoryCounter(){
		String result = Integer.toString(historyCounter);
		historyCounter++;
		return result;
	}
	protected void paintComponent(Graphics g) {
	      super.paintComponent(g);
	      Graphics2D g2 = (Graphics2D) g;
	      GradientPaint gradPaint = new GradientPaint(0, 0, SILVER, getWidth(), 0, LIGHT_BLUE);
	      g2.setPaint(gradPaint);
	      g2.fillRect(0, 0, getWidth(), getHeight());
	   }
	
	protected JPanel createPointPanel(){
		JPanel result = new JPanel();
		result.setOpaque(false);
		JLabel title = new JLabel("-------------Points-----------");
		result.add(title);
		JLabel x = new JLabel("x:");
		xPointText = new JTextField(3);
		JLabel y = new JLabel("y:");
		yPointText = new JTextField(3);
		result.add(x);
		result.add(xPointText);
		result.add(y);
		result.add(yPointText);
		JButton addPoint = new JButton("Add");
		addPoint.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					graphPanel.addPoint(Integer.parseInt(xPointText.getText()), Integer.parseInt(yPointText.getText()));
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-Input pt val err\n");
				}
			}
		});
		result.add(addPoint);
		JButton removePoint = new JButton("Remove");
		removePoint.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){

				try{
					if(!graphPanel.removePoint(Integer.parseInt(xPointText.getText()), Integer.parseInt(yPointText.getText())))
						throw new Exception();
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-No such point\n");
				}
			}
		});
		result.add(removePoint);
		
		//TEMP
		JButton cos = new JButton("Cos");
		final JTextField cosTxt = new JTextField(6);
		cos.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent evt){
				Point2D pointA = new Point2D.Double(Integer.parseInt(x1LineText.getText()), Integer.parseInt(y1LineText.getText()));
				Point2D pointB = new Point2D.Double(Integer.parseInt(x2LineText.getText()), Integer.parseInt(y2LineText.getText()));
				double A = pointB.getX() - pointA.getX();
				double B = pointA.distance(pointB);
				double cosine = ((int)((A/B)*1000))/1000.0;
				cosTxt.setText(Double.toString(cosine));
			}
		});
		
		result.add(cos);
		result.add(cosTxt);
		return result;
	}
	protected JPanel createLinePanel(){
		JPanel result = new JPanel();
		result.setOpaque(false);
		JLabel title = new JLabel("-------------Lines-----------");
		result.add(title);
		JLabel x1 = new JLabel("x1:");
		x1LineText = new JTextField(3);
		JLabel y1 = new JLabel("y1:");
		y1LineText = new JTextField(3);
		result.add(x1);
		result.add(x1LineText);
		result.add(y1);
		result.add(y1LineText);
		JLabel x2 = new JLabel("x2:");
		x2LineText = new JTextField(3);
		JLabel y2 = new JLabel("y2:");
		y2LineText = new JTextField(3);
		result.add(x2);
		result.add(x2LineText);
		result.add(y2);
		result.add(y2LineText);
		JButton addLine = new JButton("Add");
		addLine.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					Point2D pointA = new Point2D.Double(Integer.parseInt(x1LineText.getText()), Integer.parseInt(y1LineText.getText()));
					Point2D pointB = new Point2D.Double(Integer.parseInt(x2LineText.getText()), Integer.parseInt(y2LineText.getText()));
					graphPanel.addLine(pointA, pointB);
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-Input ln val err\n");
				}
			}
		});
		result.add(addLine);
		JButton removeLine = new JButton("Remove");
		removeLine.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					Point2D pointA = new Point2D.Double(Integer.parseInt(x1LineText.getText()), Integer.parseInt(y1LineText.getText()));
					Point2D pointB = new Point2D.Double(Integer.parseInt(x2LineText.getText()), Integer.parseInt(y2LineText.getText()));
					if(!graphPanel.removeLine(pointA, pointB))
						throw new Exception();
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-No such line\n");
				}
			}
		});
		result.add(removeLine);
		return result;
	}
	protected JPanel createRandPanel(){
		JPanel result = new JPanel();
		result.setOpaque(false);
		JLabel title = new JLabel("---------Randomize--------");
		result.add(title);
		final JTextField numPoints = new JTextField(3);
		JButton createRandPoints = new JButton("Rnd Pts");
		createRandPoints.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					for(int i = 0; i < Integer.parseInt(numPoints.getText()); i++){
						Random findPt = new Random(System.nanoTime());
						int randX = findPt.nextInt(graphPanel.getWidth()-15)-(graphPanel.getWidth()/2-8);
						int randY = findPt.nextInt(graphPanel.getHeight()-15)-(graphPanel.getHeight()/2-8);
							graphPanel.addPoint(randX, randY);
					}
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-Pt add err\n");
				}
			}
		});
		result.add(numPoints);
		result.add(createRandPoints);
		final JTextField numLines = new JTextField(3);
		JButton createRandLines = new JButton("Rnd Lns");
		createRandLines.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					for(int i = 0; i < Integer.parseInt(numLines.getText()); i++){
						int pointNum = graphPanel.getPoints().size();
						if(i >= pointNum){break;}
						Random findLn = new Random();
						int getPointA = findLn.nextInt(pointNum-1);
						int getPointB = findLn.nextInt(pointNum-1);
						Point2D pointA = graphPanel.getPoints().get(getPointA);
						Point2D pointB = graphPanel.getPoints().get(getPointB);
						graphPanel.addLine(pointA, pointB);
					}						
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-Ln add err\n");
				}
			}
		});
		result.add(numLines);
		result.add(createRandLines);
		return result;
	}
	protected JPanel createStatusHistoryPanel(){
		JPanel result = new JPanel(null);
		result.setOpaque(false);
		statusHistory = new JTextArea();
		statusHistory.setEditable(false);
		JScrollPane scroll = new JScrollPane(statusHistory);
		scroll.setBounds(0,0,145,100);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		result.add(scroll);
		return result;
	}
	protected JPanel createAlgPanel(){
		JPanel result = new JPanel();
		result.setOpaque(false);
		algorithmsCombo = new JComboBox(new String[]{"Choose Algorithm"});
		
		JButton run = new JButton("Run");
		run.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				//graphPanel.setAlgorithmInterrupt(false);
				try{
					long timer = startClock();
					((GraphAlgorithm) algorithmsCombo.getSelectedItem()).runAlgorithm(graphPanel);
					timer = stopClock(timer);
					statusHistory.append(getHistoryCounter()+"-Alg completed\n");
					statusHistory.append(getHistoryCounter()+"-Time: "+timer+"ms\n");
				}
				catch(Exception e){
					statusHistory.append(getHistoryCounter()+"-Err running alg\n");
				}
			}
		});
//		JButton stop = new JButton("Stop");
//		run.addActionListener(new ActionListener(){
//			public void actionPerformed(ActionEvent evt){
//				graphPanel.setAlgorithmInterrupt(true);
//			}
//		});
		
		
		JButton clearAll = new JButton("Clear All");
		clearAll.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				graphPanel.clearLines();
				graphPanel.clearPoints();
			}
		});
		
		result.add(algorithmsCombo);
		result.add(run);
//		result.add(stop);
		result.add(clearAll);
		return result;
	}
	
	//start timer uses class variable 'secondsPassed'
	private long startClock(){

		return System.nanoTime();
	}
	private long stopClock(long timer){

		long elapsedTime = System.nanoTime() - timer;
		//seconds
		//double elapsed = (double) elapsedTime / 1000000000.0;
		//milli
		double elapsed = (double) elapsedTime / 1000000.0;
		//micro
		//double elapsed = (double) elapsedTime / 1000.0;
		long result = Math.round(elapsed);
		return result;
	}

}
