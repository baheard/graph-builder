package graphBuilder;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class GraphComponent extends JComponent implements MouseListener {

	private static final int HIT_BOX_SIZE = 15;
	int width;
	int height;
	List<Point2D> points;
	List<Line2D> lines;
	Point2D selectedPointA = null;
	Point2D selectedPointB = null;
	Line2D selectedLine = null;
	Rectangle hitBox = null;
	
//	private Boolean algorithmInterrupt=false;
//	private Integer algorithmSpeed=10;
	
	//fields to update with selected points / lines
	protected JTextField xPointText = null;
	protected JTextField yPointText = null;
	protected JTextField x1LineText = null;
	protected JTextField y1LineText = null;
	protected JTextField x2LineText = null;
	protected JTextField y2LineText = null;
	
	public GraphComponent(int width, int height){
		this.width = width;
		this.height = height;
		this.setBackground(Color.blue);
		this.setPreferredSize(new Dimension(width, height));
		
		points = new ArrayList<Point2D>();
		lines = new ArrayList<Line2D>();
		
		addMouseListener(this);
		
		this.setToolTipText("Graph Component");
	}
	
//add and remove points and lines
	public void addPoint(int x, int y){
		Point2D newPoint = new Point2D.Double(x,y);
		points.add(newPoint);
		setSelectedPointA(newPoint);
		this.repaint();
	}
	public Boolean removePoint(double x, double y){
		for(Point2D point : points){
			if((int)point.getX() == (int)x && (int)point.getY() == (int)y){
				points.remove(point);
				repaint();
				return true;
			}
		}
		return false;
	}
	public Boolean removePoint(Point2D toRemove){
		return removePoint(toRemove.getX(), toRemove.getY());
	}
	public void addLine(Point2D a, Point2D b){
		Line2D newLine = new Line2D.Double(a,b);
		lines.add(newLine);
		setSelectedLine(newLine);
	}
	public void addLine(Line2D newLine){
		addLine(newLine.getP1(), newLine.getP2());
	}
	public Boolean removeLine(Point2D a, Point2D b){
		for(Line2D line : lines){
			if((line.getP1().equals(a) && line.getP2().equals(b)) 
					|| (line.getP1().equals(b) && line.getP2().equals(a))){
				lines.remove(line);
				repaint();
				return true;
			}
		}
		return false;
	}
	public Boolean removeLine(Line2D.Double toRemove){
		return removeLine(toRemove.getP1(), toRemove.getP2());
	}
	public void clearPoints(){
		points.clear();
		repaint();
	}
	public void clearLines(){
		lines.clear();
		repaint();
	}
	
	@Override
	public void paintComponent(Graphics g){
		//set up graphics
		Graphics2D g2 = (Graphics2D) g;
		//g2.setBackground(Color.WHITE);
		AffineTransform at = g2.getTransform();
		//at.translate(width/2, height/2);
		double scaleW = (double)getWidth()/width;
		double scaleH = (double)getHeight()/height;
		
		at.translate(getWidth()/2, getHeight()/2);
		at.scale(scaleW, -scaleH);
		g2.setTransform(at);
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		//draw axis
		float dash1[] = {2.0f, 8.0f};
		g2.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10, dash1, 0));
		g2.draw(new Line2D.Double(-getWidth()*(1/scaleW),0,getWidth()*(1/scaleW),0));
		g2.draw(new Line2D.Double(0,-getHeight()*(1/scaleH),0,getHeight()*(1/scaleW)));
		
		//draw dynamic points & lines
		g2.setStroke(new BasicStroke(2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		for(Point2D point : points){
			g2.setColor(Color.black);
			Ellipse2D toDraw = new Ellipse2D.Double(point.getX()-3, point.getY()-3, 6, 6);
			if(point == selectedPointA){
				g2.setColor(Color.red);	
			}
			if(point == selectedPointB){
				g2.setColor(Color.orange);	
			}
			g2.draw(toDraw);
		}
		for(Line2D line : lines){
			g2.setColor(Color.black);
			if(line == selectedLine){
				g2.setColor(Color.red);	
			}
			g2.draw(line);
		}
//		if(hitBox!=null)
//			g2.draw(hitBox);
	}
	public void paintImmediately(){
		super.paintImmediately(0,0, getWidth(), getHeight());
	}
	
//get & set
	public List<Point2D> getPoints(){return points;}
	public List<Line2D> getLines(){return lines;}
	public Point2D selectedPointA(){
		return selectedPointA;
	}
	public Point2D selectedPointB(){
		return selectedPointB;
	}
	public Line2D selectedLine(){
		return selectedLine;
	}
	public void setTextFields(	
			JTextField xPointText,
			JTextField yPointText,
			JTextField x1LineText,
			JTextField y1LineText,
			JTextField x2LineText,
			JTextField y2LineText){
		this.xPointText = xPointText;
		this.yPointText = yPointText;
		this.x1LineText = x1LineText;
		this.y1LineText = y1LineText;
		this.x2LineText = x2LineText;
		this.y2LineText = y2LineText;
		
	}
	public void setSelectedPointA(Point2D point){
		xPointText.setText(Integer.toString((int)point.getX()));
		yPointText.setText(Integer.toString((int)point.getY()));
		if(selectedPointA!=null && point!=selectedPointA) 
			selectedPointB = selectedPointA;
		selectedPointA = point;
		x1LineText.setText(Integer.toString((int)selectedPointA.getX()));
		y1LineText.setText(Integer.toString((int)selectedPointA.getY()));
		if(selectedPointB!=null)
			x2LineText.setText(Integer.toString((int)selectedPointB.getX()));
		if(selectedPointB!=null)
			y2LineText.setText(Integer.toString((int)selectedPointB.getY()));
		repaint();
	}
	public void setSelectedLine(Line2D line){
		selectedLine = line;
		x1LineText.setText(Integer.toString((int)selectedLine.getP1().getX()));
		y1LineText.setText(Integer.toString((int)selectedLine.getP1().getY()));
		x2LineText.setText(Integer.toString((int)selectedLine.getP2().getX()));
		y2LineText.setText(Integer.toString((int)selectedLine.getP2().getY()));
		repaint();
	}
//	public Boolean getAlgorithmInterrupt() {
//		return algorithmInterrupt;
//	}
//	public void setAlgorithmInterrupt(Boolean algorithmInterrupt) {
//		this.algorithmInterrupt = algorithmInterrupt;
//	}
//	public Integer getAlgorithmSpeed() {
//		return algorithmSpeed;
//	}
//	public void setAlgorithmSpeed(Integer algorithmSpeed) {
//		this.algorithmSpeed = algorithmSpeed;
//	} 

//tooltip methods
	@Override
	public Point getToolTipLocation(MouseEvent e){
		Point p = e.getPoint();
		p.y += 15;
		return p;
	}
	@Override
	public String getToolTipText(MouseEvent e)
	{
		double x = e.getPoint().getX() - getWidth()/2;
		double y = getHeight()/2 - e.getPoint().getY();
		return x+", "+y;
	}

//mousey methods
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		double x = e.getPoint().getX() - getWidth()/2;
		double y = getHeight()/2 - e.getPoint().getY();
		if(e.getButton() == MouseEvent.BUTTON2 || e.getButton() == MouseEvent.BUTTON3){
			addPoint((int)x,(int)y);
			return;
		}
		int boxX = (int)x - HIT_BOX_SIZE / 2;
		int boxY = (int)y;// + HIT_BOX_SIZE / 2;
		//Rectangle 
		hitBox = new Rectangle(boxX, boxY, HIT_BOX_SIZE, HIT_BOX_SIZE);
		int width = HIT_BOX_SIZE;
		int height = HIT_BOX_SIZE;
		for(Point2D point : points){
			if(hitBox.contains(point))			
			{
				setSelectedPointA(point);
				return;
			}
		}
		for(Line2D line : lines){
			if(line.intersects(hitBox)){
				setSelectedLine(line);
				return;
			}
		}
		repaint();
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

		
	}


}