package algorithms;
import graphBuilder.GraphAlgorithm;
import graphBuilder.GraphComponent;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.awt.Polygon;

public class QuickGrahamScanAlgorithm extends GraphAlgorithm {

	final static int WAIT = 30;
	public QuickGrahamScanAlgorithm(){
		super("Quick Graham Scan");
	}
	
	@Override
	public void runAlgorithm(GraphComponent graph){
		List<GSPoint> graphPoints = new ArrayList<GSPoint>();
		
		graph.clearLines();
		for(Point2D point : graph.getPoints()){
			graphPoints.add(new GSPoint(point));
		}
		int sum = graphPoints.size();
//		printAllPoints(graphPoints);
		
		//Delete points in the area
		graphPoints = discardSomeInternalPoints(graphPoints, sum);
//		printAllPoints(graphPoints);
		
		GSPoint startPoint = findLowestPoint(graphPoints);
		//remove init point from list of points - it is special
			

		
		applyCosine(startPoint, graphPoints);
		//cosval of start point set to -2 so it will be sorted to front
		startPoint.setCosVal(-2);
		graphPoints = quicksortPoints(graphPoints);		
		try{
			graph.addLine(graphPoints.get(0), graphPoints.get(1));
			graph.paintImmediately();
			Thread.sleep(WAIT);
			int i = 1;
			while(i < graphPoints.size()-1){
				GSPoint a = graphPoints.get(i-1);
				GSPoint b = graphPoints.get(i);
				GSPoint c = graphPoints.get(i+1);
				graph.addLine(b,c);
				graph.paintImmediately();
				Thread.sleep(WAIT);
				double turnVal = findCrossProduct(a,b,c);
				//if turnVal <= 0, it is a straight or right turn - do nothing
				//if turnVal > 0, it is a left turn - remove point B from hull
				if(turnVal >= 0){
					graphPoints.remove(i);
					graph.removeLine(a, b);
					graph.removeLine(b, c);
					graph.paintImmediately();
					Thread.sleep(WAIT);
					i--;
				}
				else i++;
			}
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		graph.addLine(graphPoints.get(graphPoints.size()-1), graphPoints.get(0));
		graph.paintImmediately();
	}
	
	public String getName(){
		return name;
	}

	private GSPoint findLowestPoint(List<GSPoint> points){
		GSPoint result = points.get(1);
		for(GSPoint point : points){
			//if y is less
			if(point.getY() < result.getY()){
				result = point;
			}
			//else if y is equal but x is less
			else if(point.getY() == result.getY()){
				if(point.getX() < result.getX()){
					result = point;
				}
			}
		}
		return result;
	}
	
	private void applyCosine(GSPoint initPoint, List<GSPoint> points){
		for(GSPoint point : points){
			point.setCos(initPoint);
		}
	}
	
	private List<GSPoint> quicksortPoints(List<GSPoint> points){
		if(points.size()<2){
			return points;
		}
		List<GSPoint> less = new ArrayList<GSPoint>();
		List<GSPoint> equal = new ArrayList<GSPoint>();
		List<GSPoint> more = new ArrayList<GSPoint>();
		double partition = points.get(1).getCosVal();
		for(GSPoint point : points){
			if(point.getCosVal() < partition)
				less.add(point);
			else if(point.getCosVal() > partition)
				more.add(point);
			else
				equal.add(point);
		}
		less = quicksortPoints(less);
		more = quicksortPoints(more);
		List<GSPoint> result = less;
		result.addAll(equal);
		result.addAll(more);
		return result;
	}
	
	private double findCrossProduct(GSPoint a, GSPoint b, GSPoint c){
		//in turn a -> b -> c
		//if cross product > 1, left turn from b -> c
		//if cross product < 1, right turn from b -> c
		//if cross product = 0, abc is colinear
		double x1 = a.getX();
		double y1 = a.getY();
		double x2 = b.getX();
		double y2 = b.getY();
		double x3 = c.getX();
		double y3 = c.getY();
		double result = ((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1));
		return result; 
	}
	
	private List<GSPoint> discardSomeInternalPoints(List<GSPoint> points, int sum) {
		
		List<GSPoint> newPoints = new ArrayList<GSPoint>();
		List<GSPoint> winPointsX = new ArrayList<GSPoint>();
		List<GSPoint> winPointsY = new ArrayList<GSPoint>();
		List<GSPoint> losePointsX = new ArrayList<GSPoint>();
		List<GSPoint> losePointsY = new ArrayList<GSPoint>();
		
		GSPoint leftPoint;
		GSPoint rightPoint;
		GSPoint upPoint;
		GSPoint lowPoint;
		
		GSPoint currentPoint;
		GSPoint nextPoint;
		
		for(int i = 0; i < sum; i=i+2) {
			
			currentPoint = points.get(i);
			nextPoint = points.get(i+1);
			if(currentPoint.getX() > nextPoint.getX()) {
				winPointsX.add(currentPoint);
				losePointsX.add(nextPoint);
			}
			else if(currentPoint.getX() == nextPoint.getX()) {
				if( currentPoint.getY() < nextPoint.getY() ) {
					winPointsX.add(nextPoint);
					losePointsX.add(currentPoint);
				}
				else {
					winPointsX.add(currentPoint);
					losePointsX.add(nextPoint);
				}
			}
			else {
				winPointsX.add(nextPoint);
				losePointsX.add(currentPoint);
			}
			
			if(currentPoint.getY() > nextPoint.getY()) {
				winPointsY.add(currentPoint);
				losePointsY.add(nextPoint);
			}
			else if(currentPoint.getY() == nextPoint.getY()) {
				if( currentPoint.getX() < nextPoint.getX() ) {
					winPointsY.add(currentPoint);
					losePointsY.add(nextPoint);
				}
				else {
					winPointsY.add(nextPoint);
					losePointsY.add(currentPoint);
				}
			}
			else {
				winPointsY.add(nextPoint);
				losePointsY.add(currentPoint);
			}
		}
			
		leftPoint = losePointsX.get(0);
		rightPoint = winPointsX.get(0);
		upPoint = winPointsY.get(0);
		lowPoint = losePointsY.get(0);
			for(int j = 0; j < sum/2; j++) {
				if(leftPoint.getX() > losePointsX.get(j).getX())
					leftPoint = losePointsX.get(j);
				else if(leftPoint.getX() == losePointsX.get(j).getX()) {
					if(leftPoint.getY() > losePointsX.get(j).getY())
						leftPoint = losePointsX.get(j);
				}
				
				if(rightPoint.getX() < winPointsX.get(j).getX())
					rightPoint = winPointsX.get(j);
				else if(rightPoint.getX() == winPointsX.get(j).getX()) {
					if(rightPoint.getY() < winPointsX.get(j).getY())
						rightPoint = winPointsX.get(j);
				}

				if(upPoint.getY() < winPointsY.get(j).getY())
					upPoint = winPointsY.get(j);
				else if(upPoint.getY() == winPointsY.get(j).getY()) {
					if(upPoint.getX() > winPointsY.get(j).getX())
						upPoint = winPointsY.get(j);
				}

				if(lowPoint.getY() > losePointsY.get(j).getY())
					lowPoint = losePointsY.get(j);
				else if(lowPoint.getY() == losePointsY.get(j).getY()) {
					if(lowPoint.getX() < losePointsY.get(j).getX())
						lowPoint = losePointsY.get(j);
				}
			}
			
//			double leftUp = (upPoint.getY() - leftPoint.getY()) / (upPoint.getX() - leftPoint.getX());
//			double lefLow = (lowPoint.getY() - leftPoint.getY()) / (lowPoint.getX() - leftPoint.getX());
//			double rightUp = (upPoint.getY() - rightPoint.getY()) / (upPoint.getX() - rightPoint.getX());
//			double rightLow = (lowPoint.getY() - rightPoint.getY()) / (lowPoint.getX() - rightPoint.getX());
			


			int[] xs = {(int)leftPoint.getX(),(int)upPoint.getX(),(int)rightPoint.getX(),(int)lowPoint.getX()};
			int[] ys = {(int)leftPoint.getY(),(int)upPoint.getY(),(int)rightPoint.getY(),(int)lowPoint.getY() };

			Polygon polygon = new Polygon(xs,ys,4);
			
			for(int m = 0; m < sum; m++) {
				currentPoint = points.get(m);
				if(currentPoint.equals(leftPoint)||currentPoint.equals(rightPoint)||currentPoint.equals(upPoint)||currentPoint.equals(lowPoint))
					newPoints.add(currentPoint);
//				else if( !polygon.contains(currentPoint) ){
////					double slopeA = (currentPoint.getY()-upPoint.getY())/(currentPoint.getX()-upPoint.getX());
////					double slopeB = (currentPoint.getY()-leftPoint.getY())/(currentPoint.getX()-leftPoint.getX());
////					double slopeC = (currentPoint.getY()-lowPoint.getY())/(currentPoint.getX()-lowPoint.getX());
//
//							
//					}
				else if( !polygon.contains(currentPoint) )
					newPoints.add(currentPoint);
			}
		return newPoints;
	}
	
	void printAllPoints(List<GSPoint> points) {
		int count = 0;
		for(GSPoint point : points) {
			System.out.println("["+point.getX()+", "+point.getY()+"]");
			count++;
		}
		System.out.println(count);
		System.out.println();
	}
}
