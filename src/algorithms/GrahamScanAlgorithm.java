package algorithms;
import graphBuilder.GraphAlgorithm;
import graphBuilder.GraphComponent;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class GrahamScanAlgorithm extends GraphAlgorithm {

	final static int WAIT = 30;
	public GrahamScanAlgorithm(){
		super("Graham Scan");
	}
	
	@Override
	public void runAlgorithm(GraphComponent graph){
		List<GSPoint> graphPoints = new ArrayList<GSPoint>();
		graph.clearLines();
		for(Point2D point : graph.getPoints()){
			graphPoints.add(new GSPoint(point));
		}
		GSPoint startPoint = findLowestPoint(graphPoints);
		//remove init point from list of points - it is special
		applyCosine(startPoint, graphPoints);
		//cosval of start point set to -2 so it will be sorted to front
		startPoint.setCosVal(-2);
		graphPoints = quicksortPoints(graphPoints);		
		try{
			graph.addLine(graphPoints.get(0), graphPoints.get(1));
			graph.paintImmediately();
			Thread.sleep(WAIT);
			int i = 1;
			while(i < graphPoints.size()-1){
				GSPoint a = graphPoints.get(i-1);
				GSPoint b = graphPoints.get(i);
				GSPoint c = graphPoints.get(i+1);
				graph.addLine(b,c);
				graph.paintImmediately();
				Thread.sleep(WAIT);
				double turnVal = findCrossProduct(a,b,c);
				//if turnVal <= 0, it is a straight or right turn - do nothing
				//if turnVal > 0, it is a left turn - remove point B from hull
				if(turnVal >= 0){
					graphPoints.remove(i);
					graph.removeLine(a, b);
					graph.removeLine(b, c);
					graph.paintImmediately();
					Thread.sleep(WAIT);
					i--;
				}
				else i++;
			}
		}
		catch(InterruptedException e){
			e.printStackTrace();
		}
		graph.addLine(graphPoints.get(graphPoints.size()-1), graphPoints.get(0));
		graph.paintImmediately();
	}
	
	public String getName(){
		return name;
	}

	private GSPoint findLowestPoint(List<GSPoint> points){
		GSPoint result = points.get(1);
		for(GSPoint point : points){
			//if y is less
			if(point.getY() < result.getY()){
				result = point;
			}
			//else if y is equal but x is less
			else if(point.getY() == result.getY()){
				if(point.getX() < result.getX()){
					result = point;
				}
			}
		}
		return result;
	}
	
	private void applyCosine(GSPoint initPoint, List<GSPoint> points){
		for(GSPoint point : points){
			point.setCos(initPoint);
		}
	}
	
	private List<GSPoint> quicksortPoints(List<GSPoint> points){
		if(points.size()<2){
			return points;
		}
		List<GSPoint> less = new ArrayList<GSPoint>();
		List<GSPoint> equal = new ArrayList<GSPoint>();
		List<GSPoint> more = new ArrayList<GSPoint>();
		double partition = points.get(1).getCosVal();
		for(GSPoint point : points){
			if(point.getCosVal() < partition)
				less.add(point);
			else if(point.getCosVal() > partition)
				more.add(point);
			else
				equal.add(point);
		}
		less = quicksortPoints(less);
		more = quicksortPoints(more);
		List<GSPoint> result = less;
		result.addAll(equal);
		result.addAll(more);
		return result;
	}
	
	private double findCrossProduct(GSPoint a, GSPoint b, GSPoint c){
		//in turn a -> b -> c
		//if cross product > 1, left turn from b -> c
		//if cross product < 1, right turn from b -> c
		//if cross product = 0, abc is colinear
		double x1 = a.getX();
		double y1 = a.getY();
		double x2 = b.getX();
		double y2 = b.getY();
		double x3 = c.getX();
		double y3 = c.getY();
		double result = ((x2-x1)*(y3-y1)-(y2-y1)*(x3-x1));
		return result;
	}
	
}
