package algorithms;

import java.awt.geom.Point2D;

public class GSPoint extends Point2D.Double {
	private double cosVal;

	public GSPoint(){};
	public GSPoint(Point2D srcPt){
		super(srcPt.getX(), srcPt.getY());
	}
	
	public void setCos(GSPoint referencePoint){
		double A = this.getX() - referencePoint.getX();
		double B = referencePoint.distance(this);
		this.setCosVal(A/B);
	}

	public double getCosVal() {
		return cosVal;
	}

	public void setCosVal(double cosVal) {
		this.cosVal = cosVal;
	}
}
